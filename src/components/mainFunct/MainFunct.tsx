import './MainFunct.css';
import { IonButton } from '@ionic/react';
import React, { useEffect, useState } from 'react';

interface ContainerProps { }

const Mainfunct: React.FC<ContainerProps> = () => {
  const [selectedEmoji, setselectedEmoji] = useState<number>(0);
  const [botchoice, setbotchoice] = useState<number>(0);
  const [resulTittle, setresulTittle] = useState<string>("");

  const [touchedEmoji, settouchedEmoji] = useState<Boolean>(false);
  const EmojiArray = ['✊', '✂️', '📋'];
  const [contador, setContador] = useState(3);
  const [showFight, setshowFight] = useState(false); 

  function handleEmojiClick(index: number) {
    setselectedEmoji(index);
    setContador(3); 
    setshowFight(false);
    settouchedEmoji(true); 
  }

  function generateRandomIndex() {
    return Math.floor(Math.random() * 3);
  }

  /* En base a la comparacion arrojo el resultado de la disputa*/
  function compareResults() {
    if (selectedEmoji === botchoice) {
      setresulTittle("Empate");
    } else if (
      (selectedEmoji === 0 && botchoice === 1) ||
      (selectedEmoji === 1 && botchoice === 2) ||
      (selectedEmoji === 2 && botchoice === 0)
    ) {
      setresulTittle("Ganaste");
    } else {
      setresulTittle("Perdiste");
    }
  }

  function restart(){
    settouchedEmoji(false)
    setshowFight(false)
  }
  

  useEffect(() => {
    if (touchedEmoji && contador === 0) {
      compareResults();
    }
  }, [botchoice]);

  useEffect(() => {
    if (touchedEmoji && contador === 0) {
      setbotchoice(generateRandomIndex());
    }
  }, [touchedEmoji, contador]);

  useEffect(() => {
    let timer: NodeJS.Timeout | null = null;
    if (touchedEmoji ==true) {
      // Inicia el contador
      timer = setInterval(() => {
        setContador((prevContador) => {
          if (prevContador === 1) {
            clearInterval(timer as NodeJS.Timeout);
            setshowFight(true);
            setbotchoice(generateRandomIndex());
            compareResults();
            return 0;
          } else {
            return prevContador - 1;
          }
        });
      }, 1000);
    }

    return () => {
      if (timer !== null) {
        clearInterval(timer);
      }
    };
  }, [touchedEmoji,contador, botchoice]);

  return (
    <div className="bg-imag">
      <div className="main-container">
        {touchedEmoji == false && (
          <div className="sub-main">
            <div>
              <p className="emoji-tittle">
                Seleccione para jugar!
              </p>
            </div>
            <div className="emoji-container">
              {EmojiArray.map((emoji, index) => (
                <p
                  key={index}
                  className={`action-main float-animation ${selectedEmoji === index ? 'selected' : ''}`}
                  onClick={() => handleEmojiClick(index)}
                >
                  {emoji}
                </p>
              ))}
            </div>
          </div>
        )}

        {touchedEmoji !== false && contador > 0 && (
          <div className="contador">
            {contador}
          </div>
        )}

        {showFight ==true && (
          <div className="game-container">
              <div className="emoji-tittle">
                {resulTittle}
              </div>
              <div className="selections">
                  <div className='User-selection'>
                    <p>{EmojiArray[selectedEmoji]}</p>
                  </div>
                  <div className="bot-selection">
                  <p>{EmojiArray[botchoice]}</p>
                  </div>
              </div>

              <div>
                <IonButton className="button-main" onClick={() => restart()} >Volver a jugar!</IonButton>
              </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Mainfunct;
