import './ExploreContainer.css';
import { IonButton } from '@ionic/react';
import { Link } from 'react-router-dom';
interface ContainerProps { }

const ExploreContainer: React.FC<ContainerProps> = () => {

  function kill() {
    window.location.href = 'https://www.youtube.com/shorts/41iWg91yFv0';
  }
  
  return (
    <div className="bg-image">
      <div className="main-container">
        <p> Listo para Jugar a piedra papel o tijeras ? </p>
        <div className='button-container'>
            
          <Link to="/main">
            <IonButton className='button-main'>Si!</IonButton>
          </Link>
          <IonButton className='button-main' onClick={kill}>Matate</IonButton>
        </div>
      </div>
    </div>
  );
};

export default ExploreContainer;
